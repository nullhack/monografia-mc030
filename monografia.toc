\select@language {portuguese}
\contentsline {chapter}{\numberline {1}Introdu\IeC {\c c}\IeC {\~a}o}{2}{chapter.1}
\contentsline {chapter}{\numberline {2}Justificativa}{4}{chapter.2}
\contentsline {chapter}{\numberline {3}Objetivos}{5}{chapter.3}
\contentsline {chapter}{\numberline {4}Bitcoin}{6}{chapter.4}
\contentsline {section}{\numberline {4.1}O que \IeC {\'e} Bitcoin}{6}{section.4.1}
\contentsline {section}{\numberline {4.2}Hist\IeC {\'o}ria}{7}{section.4.2}
\contentsline {section}{\numberline {4.3}Descri\IeC {\c c}\IeC {\~a}o da plataforma}{8}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}Transa\IeC {\c c}\IeC {\~o}es}{8}{subsection.4.3.1}
\contentsline {subsection}{\numberline {4.3.2}Servidor de carimbo}{11}{subsection.4.3.2}
\contentsline {subsection}{\numberline {4.3.3}Prova de trabalho}{12}{subsection.4.3.3}
\contentsline {subsection}{\numberline {4.3.4}Minera\IeC {\c c}\IeC {\~a}o}{13}{subsection.4.3.4}
\contentsline {subsection}{\numberline {4.3.5}Incentivo}{13}{subsection.4.3.5}
\contentsline {subsection}{\numberline {4.3.6}Verifica\IeC {\c c}\IeC {\~a}o simplificada de pagamento}{14}{subsection.4.3.6}
\contentsline {subsection}{\numberline {4.3.7}Vis\IeC {\~a}o geral}{15}{subsection.4.3.7}
\contentsline {section}{\numberline {4.4}Atualiza\IeC {\c c}\IeC {\~o}es do artigo original}{16}{section.4.4}
\contentsline {subsection}{\numberline {4.4.1}Se\IeC {\c c}\IeC {\~a}o 1 resumo}{16}{subsection.4.4.1}
\contentsline {subsection}{\numberline {4.4.2}Se\IeC {\c c}\IeC {\~a}o 2 transa\IeC {\c c}\IeC {\~o}es}{17}{subsection.4.4.2}
\contentsline {subsection}{\numberline {4.4.3}Se\IeC {\c c}\IeC {\~a}o 4 prova de trabalho}{17}{subsection.4.4.3}
\contentsline {subsection}{\numberline {4.4.4}Se\IeC {\c c}\IeC {\~a}o 7 recuperando espa\IeC {\c c}o em disco}{18}{subsection.4.4.4}
\contentsline {subsection}{\numberline {4.4.5}Se\IeC {\c c}\IeC {\~a}o 8 verifica\IeC {\c c}\IeC {\~a}o simplificada de pagamento}{18}{subsection.4.4.5}
\contentsline {chapter}{\numberline {5}Discuss\IeC {\~a}o}{19}{chapter.5}
\contentsline {section}{\numberline {5.1}Usos para a cadeia de blocos}{19}{section.5.1}
\contentsline {section}{\numberline {5.2}Alguns pontos de debate}{20}{section.5.2}
\contentsline {subsection}{\numberline {5.2.1}Instabilidade de pre\IeC {\c c}os}{20}{subsection.5.2.1}
\contentsline {subsection}{\numberline {5.2.2}Irreversibilidade de transa\IeC {\c c}\IeC {\~o}es e media\IeC {\c c}\IeC {\~a}o de disputas}{20}{subsection.5.2.2}
\contentsline {subsection}{\numberline {5.2.3}Perspectiva e desafios para economia}{20}{subsection.5.2.3}
\contentsline {subsection}{\numberline {5.2.4}Quest\IeC {\~a}o energ\IeC {\'e}tica}{21}{subsection.5.2.4}
\contentsline {section}{\numberline {5.3}Sistema de vota\IeC {\c c}\IeC {\~a}o distribu\IeC {\'\i }da no mundo real}{21}{section.5.3}
\contentsline {subsection}{\numberline {5.3.1}Democracias em uso e a democracia l\IeC {\'\i }quida}{21}{subsection.5.3.1}
\contentsline {subsection}{\numberline {5.3.2}Democracia direta}{22}{subsection.5.3.2}
\contentsline {subsection}{\numberline {5.3.3}Democracia representativa}{22}{subsection.5.3.3}
\contentsline {subsection}{\numberline {5.3.4}Democracia l\IeC {\'\i }quida}{23}{subsection.5.3.4}
\contentsline {chapter}{\numberline {6}Resultados}{26}{chapter.6}
\contentsline {section}{\numberline {6.1}\textit {basiccoin}}{26}{section.6.1}
\contentsline {paragraph}{cli.py}{26}{paragraph*.2}
\contentsline {paragraph}{blockchain.py}{26}{paragraph*.3}
\contentsline {paragraph}{miner.py}{27}{paragraph*.4}
\contentsline {section}{\numberline {6.2}\textit {Block Chain Based Vote System}}{27}{section.6.2}
\contentsline {paragraph}{custom.py}{28}{paragraph*.5}
\contentsline {paragraph}{Uma prova de conceito utilizando o protocolo modificado}{28}{paragraph*.6}
\contentsline {paragraph}{Uma aplica\IeC {\c c}\IeC {\~a}o \IeC {\`a} realidade brasileira:}{35}{paragraph*.7}
\contentsline {chapter}{\numberline {7}Conclus\IeC {\~a}o}{37}{chapter.7}
\contentsline {chapter}{Bibliografia}{38}{chapter.7}
\contentsline {chapter}{\numberline {A}Vocabul\IeC {\'a}rio}{40}{appendix.A}
\contentsline {paragraph}{Assinatura}{40}{paragraph*.9}
\contentsline {paragraph}{Bip}{40}{paragraph*.10}
\contentsline {paragraph}{Bit}{40}{paragraph*.11}
\contentsline {paragraph}{Bitcoin}{40}{paragraph*.12}
\contentsline {paragraph}{Bloco}{40}{paragraph*.13}
\contentsline {paragraph}{Bloco g\IeC {\^e}nesis}{40}{paragraph*.14}
\contentsline {paragraph}{BTC}{40}{paragraph*.15}
\contentsline {paragraph}{Cadeia de blocos}{40}{paragraph*.16}
\contentsline {paragraph}{Calibragem da meta de dificuldade}{40}{paragraph*.17}
\contentsline {paragraph}{Carteira}{40}{paragraph*.18}
\contentsline {paragraph}{Chave privada}{41}{paragraph*.19}
\contentsline {paragraph}{Chave p\IeC {\'u}blica}{41}{paragraph*.20}
\contentsline {paragraph}{Confirma\IeC {\c c}\IeC {\~a}o}{41}{paragraph*.21}
\contentsline {paragraph}{Criptografia}{41}{paragraph*.22}
\contentsline {paragraph}{Dificuldade}{41}{paragraph*.23}
\contentsline {paragraph}{Endere\IeC {\c c}o}{41}{paragraph*.24}
\contentsline {paragraph}{Gasto duplo}{41}{paragraph*.25}
\contentsline {paragraph}{Hash}{41}{paragraph*.26}
\contentsline {paragraph}{Minera\IeC {\c c}\IeC {\~a}o}{42}{paragraph*.27}
\contentsline {paragraph}{Minerador}{42}{paragraph*.28}
\contentsline {paragraph}{ponto a ponto - P2P}{42}{paragraph*.29}
\contentsline {paragraph}{Prova de trabalho}{42}{paragraph*.30}
\contentsline {paragraph}{Recompensa}{42}{paragraph*.31}
\contentsline {paragraph}{Rede}{42}{paragraph*.32}
\contentsline {paragraph}{Taxa de hash}{42}{paragraph*.33}
\contentsline {paragraph}{Taxa de processamento de transa\IeC {\c c}\IeC {\~a}o}{42}{paragraph*.34}
\contentsline {paragraph}{Transa\IeC {\c c}\IeC {\~a}o}{42}{paragraph*.35}
